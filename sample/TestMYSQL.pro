#-------------------------------------------------
#
# Project created by QtCreator 2014-09-24T09:24:24
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TestMYSQL
TEMPLATE = app


SOURCES += main.cpp\
        testmysql.cpp

HEADERS  += testmysql.h

FORMS    += testmysql.ui
