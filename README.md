# MariaDB Native Client for Android #

More info: [http://munteanumarian.blogspot.com/2014/09/build-mysql-plugin-for-qt-android.html](http://munteanumarian.blogspot.com/2014/09/build-mysql-plugin-for-qt-android.html)

Sample app running on Android 2.3:  
![](http://munteanu-marian.appspot.com/files/android/TestMYSQL_android.JPG)  

![](http://munteanu-marian.appspot.com/logo.png?mariadb-client-for-android)