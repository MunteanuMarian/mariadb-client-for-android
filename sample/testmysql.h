#ifndef TESTMYSQL_H
#define TESTMYSQL_H

#include <QMainWindow>
#include <QtSql>

namespace Ui {
class TestMYSQL;
}

class TestMYSQL : public QMainWindow
{
    Q_OBJECT

    bool logat;

    QSqlDatabase db;

public:
    explicit TestMYSQL(QWidget *parent = 0);
    ~TestMYSQL();

private slots:
    void on_pushConect_clicked();

    void on_pushExec_clicked();

    void on_actionDespre_triggered();

private:
    Ui::TestMYSQL *ui;
};

#endif // TESTMYSQL_H
