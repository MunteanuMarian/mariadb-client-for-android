#include "testmysql.h"
#include "ui_testmysql.h"
#include <QMessageBox>

TestMYSQL::TestMYSQL(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TestMYSQL)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QMYSQL");
    logat = false;
    ui->pushExec->setDisabled(true);
}

TestMYSQL::~TestMYSQL()
{
    delete ui;
}

void TestMYSQL::on_pushConect_clicked()
{
    if(db.isOpen()) db.close();
    logat = false;
    db.setHostName(ui->lineServer->text());
    db.setPort(ui->linePort->text().toInt());
    db.setUserName(ui->lineUser->text());
    db.setPassword(ui->linePass->text());
    if(db.open())
    {
        QSqlQuery q = db.exec("SELECT USER() LIMIT 1");
        while (q.next())
        {
            if(q.isValid())
            {
                logat = true;
                QMessageBox::information(this, "CONEXIUNE", "Conexiune OK");
                ui->pushExec->setEnabled(true);
            }
        }
    }
    if(!logat)
    {
        ui->pushExec->setDisabled(true);
        QMessageBox::critical(this, "EROARE", db.lastError().text()
                                     + "\n\n\"Drivere\" disponibile: " + QSqlDatabase::drivers().join(", "));
    }
}

void TestMYSQL::on_pushExec_clicked()
{
    if((!logat) || (!db.isOpen()) || (!db.isValid()))
    {
        QMessageBox::critical(this, "EROARE", "Nu sunteți conectat la server.");
        return;
    }
    ui->pushExec->setDisabled(true);

    ui->tableRezultat->setColumnCount(0);
    ui->tableRezultat->setRowCount(0);

    QSqlQuery q = db.exec(ui->lineSQL->text());
    q.isValid();
    ui->tableRezultat->setColumnCount(q.record().count());
    ui->tableRezultat->setRowCount(q.size());
    for (int i = 0; i < ui->tableRezultat->columnCount(); ++i)
    {
        ui->tableRezultat->setHorizontalHeaderItem(i, new QTableWidgetItem(q.record().fieldName(i)));
    }
    QList<QList<QString> > rezultat;
    while (q.next())
    {
        if(q.isValid())
        {
            QList<QString> rand;
            for(int i = 0; i < q.record().count(); ++i)
                rand.append(q.value(i).toString());
            rezultat.append(rand);
        }
    }
    for (int r = 0; r < ui->tableRezultat->rowCount(); ++r)
    {
        for (int c = 0; c < ui->tableRezultat->columnCount(); ++c)
            ui->tableRezultat->setItem(r, c, new QTableWidgetItem(rezultat.at(r).at(c)));
    }

    ui->pushExec->setEnabled(true);
}

void TestMYSQL::on_actionDespre_triggered()
{
    QMessageBox::aboutQt(this);
}
